defmodule Solution do
  def do_line(line) do
    digits = line |> String.downcase() |> scan_digits()

    case digits do
      [] ->
        0

      _ ->
        first = List.first(digits)
        last = List.last(digits)
        String.to_integer("#{first}#{last}")
    end
  end

  def scan_digits(""), do: []

  def scan_digits(<<char::utf8, rest::binary>>) when char in ?0..?9 do
    [char - ?0 | scan_digits(rest)]
  end

  def scan_digits(str) do
    digit_words = %{
      "one" => 1,
      "two" => 2,
      "three" => 3,
      "four" => 4,
      "five" => 5,
      "six" => 6,
      "seven" => 7,
      "eight" => 8,
      "nine" => 9
    }

    digit? =
      Enum.find_value(digit_words, fn {word, digit} ->
        String.starts_with?(str, word) && digit
      end)

    rest = String.slice(str, 1..-1)

    case digit? do
      nil ->
        scan_digits(rest)

      digit ->
        [digit | scan_digits(rest)]
    end
  end
end

File.stream!("input", [:utf8, :read_ahead], :line)
|> Enum.map(&Solution.do_line/1)
|> Enum.sum()
|> IO.puts()
