defmodule Progress do
  def print_progress(nb_done, total, curr_val) do
    if nb_done >= total - 1 do
      IO.write("\r\x1b[2K")
    else
      chars = 50
      progress = round(nb_done / total * chars)
      IO.write("\r\x1b[2K#{String.duplicate(">", progress)} #{curr_val}\r")
    end
  end
end

defmodule Grid do
  @enforce_keys [:width, :height, :map]
  defstruct [:width, :height, :map]

  def parse_input(lines) do
    width = String.length(Enum.at(lines, 0))
    height = Enum.count(lines)

    map =
      Enum.reduce(Enum.with_index(lines), %{}, fn {line, y}, map ->
        Enum.reduce(Enum.with_index(String.graphemes(line)), map, fn {char, x}, map ->
          cost = String.to_integer(char)
          Map.put(map, {x, y}, cost)
        end)
      end)

    %__MODULE__{width: width, height: height, map: map}
  end

  def at(grid, {x, y}) do
    Map.get(grid.map, {x, y})
  end
end

defmodule SearchHead do
  @enforce_keys [:pos, :direction, :straight_step_count, :curr_cost, :trail]
  defstruct [:pos, :direction, :straight_step_count, :curr_cost, :trail]

  @type direction() :: :up | :down | :left | :right

  def key(%__MODULE__{pos: pos, direction: direction, straight_step_count: straight_step_count}) do
    {pos, direction, straight_step_count}
  end

  def cost(%__MODULE__{curr_cost: c}) do
    c
  end

  def new(key, cost) do
    %__MODULE__{
      pos: key.pos,
      direction: key.direction,
      straight_step_count: key.straight_step_count,
      curr_cost: cost,
      trail: []
    }
  end

  def advance(curr, grid, direction, min_straight, max_straight) do
    {curr_x, curr_y} = curr.pos

    next_pos =
      case direction do
        :up -> {curr_x, curr_y - 1}
        :down -> {curr_x, curr_y + 1}
        :left -> {curr_x - 1, curr_y}
        :right -> {curr_x + 1, curr_y}
      end

    {next_x, next_y} = next_pos

    hit_min_straight = curr.straight_step_count < min_straight and curr.direction != direction
    hit_max_straight = curr.straight_step_count >= max_straight and curr.direction == direction

    if not Enum.any?([
         is_opposite(curr.direction, direction),
         hit_min_straight,
         hit_max_straight
       ]) and
         in_bounds(next_x, next_y, grid.width, grid.height) do
      next_cost = cost(curr) + Grid.at(grid, next_pos)

      next_straight_step_count =
        if curr.direction == direction do
          curr.straight_step_count + 1
        else
          1
        end

      %__MODULE__{
        pos: next_pos,
        direction: direction,
        straight_step_count: next_straight_step_count,
        curr_cost: next_cost,
        trail: [curr | curr.trail]
      }
    else
      nil
    end
  end

  def advance_all(curr, grid, min_straight, max_straight) do
    Enum.map([:up, :down, :left, :right], fn dir ->
      advance(curr, grid, dir, min_straight, max_straight)
    end)
    |> Enum.filter(&(&1 != nil))
  end

  defp in_bounds(x, y, w, h) do
    x >= 0 and x < w and y >= 0 and y < h
  end

  defp is_opposite(dir1, dir2) do
    case {dir1, dir2} do
      {:up, :down} -> true
      {:down, :up} -> true
      {:left, :right} -> true
      {:right, :left} -> true
      _ -> false
    end
  end

  def new_origin(direction) do
    new(
      %{
        pos: {0, 0},
        direction: direction,
        straight_step_count: 1
      },
      0
    )
  end
end

defmodule Dijkstra do
  require Logger
  defstruct [:curr_cost_heads_pq, :visited_keys, :target]

  def new(grid) do
    %__MODULE__{
      curr_cost_heads_pq:
        PriorityQueue.new()
        |> PriorityQueue.push(SearchHead.new_origin(:right), 0)
        |> PriorityQueue.push(SearchHead.new_origin(:down), 0),
      visited_keys: MapSet.new(),
      target: {grid.width - 1, grid.height - 1}
    }
  end

  def iter(self, grid, min_straight, max_straight) do
    %__MODULE__{
      curr_cost_heads_pq: cost_heads_pq,
      visited_keys: visited_keys,
      target: target
    } = self

    {{:value, curr_head}, cost_heads_pq} = PriorityQueue.pop(cost_heads_pq)

    Progress.print_progress(
      MapSet.size(visited_keys),
      grid.width * grid.height * 4 * max_straight,
      SearchHead.cost(curr_head)
    )

    cond do
      MapSet.member?(visited_keys, SearchHead.key(curr_head)) ->
        iter(
          %__MODULE__{self | curr_cost_heads_pq: cost_heads_pq},
          grid,
          min_straight,
          max_straight
        )

      curr_head.pos == target and curr_head.straight_step_count >= min_straight ->
        trail = [curr_head | curr_head.trail]
        print_trail(trail, grid)
        curr_head.curr_cost

      true ->
        new_heads = SearchHead.advance_all(curr_head, grid, min_straight, max_straight)

        cost_heads_pq =
          Enum.reduce(new_heads, cost_heads_pq, fn new_head, cost_heads_pq ->
            PriorityQueue.push(cost_heads_pq, new_head, SearchHead.cost(new_head))
          end)

        iter(
          %__MODULE__{
            self
            | curr_cost_heads_pq: cost_heads_pq,
              visited_keys: MapSet.put(visited_keys, SearchHead.key(curr_head))
          },
          grid,
          min_straight,
          max_straight
        )
    end
  end

  defp print_trail(trail, grid) do
    # IO.inspect(
    #   Enum.reverse(
    #     trail
    #     |> Enum.map(&{&1.pos, &1.direction, &1.straight_step_count, &1.curr_cost})
    #   )
    # )

    trail = MapSet.new(trail |> Enum.map(& &1.pos))

    for y <- 0..(grid.height - 1) do
      for x <- 0..(grid.width - 1) do
        c = Grid.at(grid, {x, y})

        case MapSet.member?(trail, {x, y}) do
          true -> "\x1b[38;5;0m\x1b[4m#{c}\x1b[0m"
          false -> "\x1b[2;38;5;7m#{c}\x1b[0m"
        end
      end
      |> Enum.join()
      |> IO.puts()
    end
  end
end

defmodule Solution do
  def solve_part_1(grid) do
    Dijkstra.new(grid)
    |> Dijkstra.iter(grid, 1, 3)
  end

  def solve_part_2(grid) do
    Dijkstra.new(grid)
    |> Dijkstra.iter(grid, 4, 10)
  end
end

grid =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Grid.parse_input()

IO.puts("Part 1: #{Solution.solve_part_1(grid)}")
IO.puts("Part 2: #{Solution.solve_part_2(grid)}")
