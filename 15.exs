defmodule Solution do
  alias Aja.Vector, as: Vec

  def h(str) do
    Enum.reduce(:binary.bin_to_list(str), 0, fn char, acc ->
      rem((acc + char) * 17, 256)
    end)
  end

  def solve_part_1(inputs) do
    inputs
    |> Enum.map(&h/1)
    |> Enum.sum()
  end

  def parse_op(input) do
    if String.ends_with?(input, "-") do
      {:remove, String.slice(input, 0..-2)}
    else
      [_, key, value] = Regex.run(~r/^([^=]+)=(\d+)$/, input)
      {:add, key, String.to_integer(value)}
    end
  end

  def solve_part_2(inputs) do
    inputs = inputs |> Enum.map(&parse_op/1)

    boxes =
      Enum.reduce(inputs, %{}, fn op, boxes ->
        hash = elem(op, 1) |> h()

        case op do
          {:add, key, value} ->
            existing_chain = Map.get(boxes, hash, Vec.new())
            existing_index = Aja.Enum.find_index(existing_chain, fn {k, _} -> k == key end)

            new_chain =
              if existing_index != nil do
                Vec.replace_at(existing_chain, existing_index, {key, value})
              else
                Vec.append(existing_chain, {key, value})
              end

            Map.put(boxes, hash, new_chain)

          {:remove, key} ->
            existing_chain = Map.get(boxes, hash, Vec.new())
            new_chain = Aja.Enum.reject(existing_chain, fn {k, _} -> k == key end) |> Vec.new()

            Map.put(boxes, hash, new_chain)
        end
      end)

    focusing_power(boxes)
  end

  def focusing_power(boxes) do
    Enum.reduce(boxes, 0, fn {idx, chain}, acc ->
      Enum.reduce(Enum.with_index(chain), acc, fn {{_key, value}, slot_idx}, acc ->
        acc + (1 + idx) * (slot_idx + 1) * value
      end)
    end)
  end
end

inputs =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Enum.join("")
  |> String.split(",")

IO.puts("Part 1: #{Solution.solve_part_1(inputs)}")
IO.puts("Part 1: #{Solution.solve_part_2(inputs)}")
