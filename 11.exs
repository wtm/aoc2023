defmodule Solution do
  alias Aja.Vector, as: Vec

  defmodule Matrix do
    @enforce_keys [:width, :height, :map, :nb_stars]
    @type t :: %__MODULE__{
            width: non_neg_integer(),
            height: non_neg_integer(),
            map: map(),
            nb_stars: non_neg_integer()
          }
    defstruct [:width, :height, :map, :nb_stars]
  end

  def read_matrix(lines) do
    width = Enum.at(lines, 0) |> String.length()
    height = Enum.count(lines)

    {map, nb_stars} =
      Enum.reduce(Enum.with_index(lines), {%{}, 0}, fn {line, y}, acc ->
        Enum.reduce(Enum.with_index(String.graphemes(line)), acc, fn {char, x},
                                                                     {map, next_star_id} ->
          case char do
            "." ->
              {map, next_star_id}

            "#" ->
              {Map.put(map, {x, y}, next_star_id), next_star_id + 1}

            _ ->
              raise("Unknown char #{char}")
          end
        end)
      end)

    %Matrix{width: width, height: height, map: map, nb_stars: nb_stars}
  end

  @spec do_expansion(Matrix.t(), pos_integer()) :: Matrix.t()
  def do_expansion(input, factor) do
    factor = factor - 1
    %Matrix{width: width, height: height, map: map} = input

    empty_rows =
      Enum.filter(0..(height - 1), fn y ->
        Enum.all?(0..(width - 1), fn x ->
          !Map.has_key?(map, {x, y})
        end)
      end)
      |> Vec.new()

    empty_cols =
      Enum.filter(0..(width - 1), fn x ->
        Enum.all?(0..(height - 1), fn y ->
          !Map.has_key?(map, {x, y})
        end)
      end)
      |> Vec.new()

    map =
      Enum.map(map, fn {{x, y}, id} ->
        new_x = x + Aja.Enum.count(empty_cols, &(&1 < x)) * factor
        new_y = y + Aja.Enum.count(empty_rows, &(&1 < y)) * factor
        {{new_x, new_y}, id}
      end)

    %Matrix{
      width: width + Vec.size(empty_cols) * factor,
      height: height + Vec.size(empty_rows) * factor,
      map: map,
      nb_stars: input.nb_stars
    }
  end

  def get_stars_to_xy(matrix) do
    matrix.map |> Enum.map(fn {coords, id} -> {id, coords} end) |> Enum.into(%{})
  end

  def get_unordered_pairs(elements) do
    case elements do
      [] ->
        []

      [_] ->
        []

      [h | rest] ->
        rest
        |> Enum.map(fn e -> {h, e} end)
        |> Enum.concat(get_unordered_pairs(rest))
    end
  end

  def solve_expanded_map(map) do
    map
    |> get_stars_to_xy()
    |> Enum.to_list()
    |> get_unordered_pairs()
    |> Enum.map(fn {{_, {x1, y1}}, {_, {x2, y2}}} ->
      abs(x2 - x1) + abs(y2 - y1)
    end)
    |> Enum.sum()
  end
end

map =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Enum.to_list()
  |> Solution.read_matrix()

map
|> Solution.do_expansion(2)
|> Solution.solve_expanded_map()
|> (&IO.puts("Part 1: #{&1}")).()

map
|> Solution.do_expansion(1_000_000)
|> Solution.solve_expanded_map()
|> (&IO.puts("Part 2: #{&1}")).()
