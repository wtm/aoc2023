defmodule Solution do
  def frequencies(hand) do
    Enum.frequencies(hand) |> Enum.map(fn {_, count} -> count end) |> Enum.sort()
  end

  def raw_hand_kind_rank(hand) do
    freq = frequencies(hand)

    case freq do
      [1, 1, 1, 1, 1] -> 1
      [1, 1, 1, 2] -> 2
      [1, 2, 2] -> 3
      [1, 1, 3] -> 4
      [2, 3] -> 5
      [1, 4] -> 6
      [5] -> 7
    end
  end

  def parse_input_line(line) do
    [hand_str, bid] = String.split(line, " ")
    bid = String.to_integer(bid)
    hand = String.graphemes(hand_str)

    if Enum.count(hand) != 5 do
      raise("Invalid hand: #{hand_str}")
    end

    %{hand: hand, bid: bid}
  end

  def compare_hands(a, b, card_rank_fn, hand_kind_rank_fn) do
    a_rank = hand_kind_rank_fn.(a)
    b_rank = hand_kind_rank_fn.(b)

    if a_rank != b_rank do
      a_rank <= b_rank
    else
      a_crs = a |> Enum.map(card_rank_fn)
      b_crs = b |> Enum.map(card_rank_fn)
      a_crs <= b_crs
    end
  end

  def get_winnings(parsed_input, compare_hands_fn) do
    parsed_input
    |> Enum.sort(fn %{hand: a}, %{hand: b} -> compare_hands_fn.(a, b) end)
    |> Enum.with_index(1)
    |> Enum.map(fn {%{bid: bid}, rank} -> bid * rank end)
    |> Enum.sum()
  end
end

defmodule Solution.Part1 do
  import Solution

  def card_rank(card) do
    labels = "23456789TJQKA" |> String.graphemes()
    Enum.find_index(labels, &(&1 == card)) || raise("Invalid card: #{card}")
  end

  def compare_hands(a, b) do
    compare_hands(a, b, &card_rank/1, &raw_hand_kind_rank/1)
  end
end

defmodule Solution.Part2 do
  import Solution

  def card_rank(card) do
    labels = "J23456789TQKA" |> String.graphemes()
    Enum.find_index(labels, &(&1 == card)) || raise("Invalid card: #{card}")
  end

  def hand_kind_rank_J_substitution(hand) do
    non_J_cards = hand |> Enum.filter(fn card -> card != "J" end)
    _hand_kind_rank_J_substitution(non_J_cards)
  end

  defp _hand_kind_rank_J_substitution(hand_without_J) do
    case Enum.count(hand_without_J) do
      5 ->
        raw_hand_kind_rank(hand_without_J)

      x when x in 0..4 ->
        "23456789TQKA"
        |> String.graphemes()
        |> Enum.map(fn card -> [card | hand_without_J] end)
        |> Enum.map(fn hand -> _hand_kind_rank_J_substitution(hand) end)
        |> Enum.max()
    end
  end

  def compare_hands(a, b) do
    compare_hands(a, b, &card_rank/1, &hand_kind_rank_J_substitution/1)
  end
end

parsed_input =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Enum.map(&Solution.parse_input_line/1)

parsed_input
|> Solution.get_winnings(&Solution.Part1.compare_hands/2)
|> (&IO.puts("Part 1: #{&1}")).()

parsed_input
|> Solution.get_winnings(&Solution.Part2.compare_hands/2)
|> (&IO.puts("Part 2: #{&1}")).()
