defmodule Progress do
  def print_progress(nb_done, total, curr_val) do
    if nb_done >= total - 1 do
      IO.write("\r\x1b[2K")
    else
      chars = 50
      progress = round(nb_done / total * chars)
      IO.write("\r\x1b[2K#{String.duplicate(">", progress)} #{curr_val}\r")
    end
  end
end

defmodule Solution do
  def read_line(line) do
    # D 8 (#408643)
    [_, direction, distance, color] = Regex.run(~r/^(R|L|U|D) (\d+) \((#[0-9a-f]{6})\)$/, line)
    %{dir: direction, dist: String.to_integer(distance), color: color}
  end

  def construct_map(inputs) do
    map = %{{0, 0} => :initial}

    {map, _} =
      Enum.reduce(inputs, {map, {0, 0}}, fn input, {map, cursor} ->
        %{dir: dir, dist: dist, color: color} = input
        trail = follow_direction(dir, dist, cursor)

        Enum.reduce(trail, {map, cursor}, fn new_cursor, {map, _} ->
          new_map = Map.put(map, new_cursor, color)
          {new_map, new_cursor}
        end)
      end)

    {min_x, min_y, max_x, max_y} =
      Map.keys(map)
      |> Enum.reduce({0, 0, 0, 0}, fn {x, y}, {min_x, min_y, max_x, max_y} ->
        {min(min_x, x), min(min_y, y), max(max_x, x), max(max_y, y)}
      end)

    %{map: map, min_x: min_x, min_y: min_y, max_x: max_x, max_y: max_y}
  end

  def follow_direction(dir, dist, curr_cursor) do
    if dist == 0 do
      []
    else
      {x, y} = curr_cursor

      new_cursor =
        case dir do
          "L" -> {x - 1, y}
          "U" -> {x, y - 1}
          "R" -> {x + 1, y}
          "D" -> {x, y + 1}
        end

      [new_cursor | follow_direction(dir, dist - 1, new_cursor)]
    end
  end

  def print_map(constructed) do
    for y <- constructed.min_y..constructed.max_y do
      for x <- constructed.min_x..constructed.max_x do
        color = Map.get(constructed.map, {x, y})

        case color do
          nil -> "."
          :initial -> "O"
          hex -> escape_color(hex)
        end
      end
      |> Enum.join("")
      |> IO.puts()
    end
  end

  def escape_color(hex) do
    [_, rs, gs, bs] = Regex.run(~r/^#(..)(..)(..)$/, hex)
    [r, g, b] = [rs, bs, gs] |> Enum.map(fn h -> String.to_integer(h, 16) end)
    "\x1b[38;2;#{r};#{g};#{b}m#\x1b[0m"
  end

  def flood_fill(queue, next_func, visited, total) do
    if queue == [] do
      size = MapSet.size(visited)
      Progress.print_progress(size, size, size)
      visited
    else
      [next | rest] = queue
      visited = MapSet.put(visited, next)
      size = MapSet.size(visited)
      Progress.print_progress(size, total, size)
      nexts = next_func.(next) |> Enum.reject(&MapSet.member?(visited, &1))
      queue = Enum.concat(nexts, rest)
      flood_fill(queue, next_func, visited, total)
    end
  end

  def solve_part_1(constructed) do
    %{min_x: min_x, min_y: min_y, max_x: max_x, max_y: max_y} = constructed
    min_x = min_x - 1
    min_y = min_y - 1
    max_x = max_x + 1
    max_y = max_y + 1
    total = (max_x - min_x + 1) * (max_y - min_y + 1)

    nexts = fn {x, y} ->
      [
        {x - 1, y},
        {x + 1, y},
        {x, y - 1},
        {x, y + 1}
      ]
      |> Enum.filter(fn {x, y} ->
        x >= min_x and x <= max_x and y >= min_y and y <= max_y and
          not Map.has_key?(constructed.map, {x, y})
      end)
    end

    exterior = flood_fill([{min_x, min_y}], nexts, MapSet.new(), total)
    count = MapSet.size(exterior)
    dbg(count)
    interior = total - count
    interior
  end
end

constructed =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Stream.map(&Solution.read_line/1)
  |> Solution.construct_map()

IO.puts("Part 1: #{Solution.solve_part_1(constructed)}")
