defmodule Solution do
  alias Aja.Vector, as: Vec

  def parse_pipe_map(lines) do
    height = Enum.count(lines)
    width = Enum.at(lines, 0) |> String.length()

    Enum.reduce(Enum.with_index(lines), %{start_point: nil}, fn {line, y}, map ->
      Enum.reduce(Enum.with_index(String.graphemes(line)), map, fn {char, x}, map ->
        {t, connected_to} =
          case char do
            "." -> {:ground, []}
            "|" -> {:v, [{x, y - 1}, {x, y + 1}]}
            "-" -> {:h, [{x - 1, y}, {x + 1, y}]}
            "L" -> {:tr, [{x, y - 1}, {x + 1, y}]}
            "J" -> {:tl, [{x, y - 1}, {x - 1, y}]}
            "7" -> {:bl, [{x, y + 1}, {x - 1, y}]}
            "F" -> {:br, [{x, y + 1}, {x + 1, y}]}
            # specific to this input
            "S" -> {:tr, [{x, y - 1}, {x + 1, y}]}
          end

        map =
          if char == "S" do
            Map.put(map, :start_point, {x, y})
          else
            map
          end

        Map.put(map, {x, y}, {t, connected_to})
      end)
    end)
    |> Map.put(:width, width)
    |> Map.put(:height, height)
  end

  def traverse(map, starting_point) do
    traverse(map, starting_point, starting_point, nil)
  end

  def traverse(map, starting_point, current_point, prev_point) do
    next = elem(map[current_point], 1) |> Enum.filter(&(&1 != prev_point))

    cond do
      next == [] ->
        raise("No next point found")

      Enum.member?(next, starting_point) ->
        [current_point]

      true ->
        [next | _] = next
        [current_point | traverse(map, starting_point, next, current_point)]
    end
  end

  def must_div_2(n) do
    if rem(n, 2) == 0 do
      div(n, 2)
    else
      raise("Not divisible by 2")
    end
  end

  # For Part 2, I have deliberately gone with a more complicated solution
  # involving scanning each line from left to right, to practice spacial
  # reasoning and the even-odd rule in path processing. Reader should note that
  # there is a much simpler solution involving just doing a graph traversal and
  # counting the number of tiles visited that is not on the trail.

  def trail_to_yxmap(trail, map) do
    Enum.reduce(trail, %{}, fn {x, y}, yxmap ->
      Map.get(yxmap, y, Vec.new()) |> Vec.append(x) |> (&Map.put(yxmap, y, &1)).()
    end)
    |> Map.new(fn {k, v} -> {k, Vec.sort(v)} end)
  end

  defp get_map_tile(map, {x, y}) do
    elem(map[{x, y}], 0)
  end

  defp _scan_horizontal_wall(map, []), do: []

  defp _scan_horizontal_wall(map, tiles) do
    first = get_map_tile(map, hd(tiles))
    x = elem(hd(tiles), 0)
    rest = tl(tiles)

    case first do
      :v -> [{:split, x, x} | _scan_horizontal_wall(map, rest)]
      :tr -> _scan_horizontal_wall_continue(map, rest, x, :top)
      :br -> _scan_horizontal_wall_continue(map, rest, x, :bottom)
      _ -> raise("Unexpected #{first} tile at #{inspect(hd(tiles))} during left-to-right scan")
    end
  end

  defp _scan_horizontal_wall_continue(map, tiles, start_x, start_direction) do
    if tiles == [] do
      raise("Unexpected end of tiles during left-to-right scan")
    end

    curr = get_map_tile(map, hd(tiles))
    x = elem(hd(tiles), 0)
    rest = tl(tiles)

    case curr do
      :h ->
        _scan_horizontal_wall_continue(map, rest, start_x, start_direction)

      :tl ->
        case start_direction do
          :top -> [{:nonsplit, start_x, x} | _scan_horizontal_wall(map, rest)]
          :bottom -> [{:split, start_x, x} | _scan_horizontal_wall(map, rest)]
        end

      :bl ->
        case start_direction do
          :top -> [{:split, start_x, x} | _scan_horizontal_wall(map, rest)]
          :bottom -> [{:nonsplit, start_x, x} | _scan_horizontal_wall(map, rest)]
        end

      _ ->
        raise("Unexpected #{curr} tile at #{inspect(hd(tiles))} during left-to-right scan")
    end
  end

  def scan_line(map, y, xs) do
    walls = _scan_horizontal_wall(map, Aja.Enum.map(xs, fn x -> {x, y} end))

    {count, nil} =
      Enum.reduce(walls, {0, nil}, fn {split_or_nonsplit, startx, endx}, {count, curr_right} ->
        case {curr_right, split_or_nonsplit} do
          {nil, :split} ->
            {count, endx}

          {nil, :nonsplit} ->
            {count, nil}

          {_, :split} ->
            {count + (startx - 1 - (curr_right + 1) + 1), nil}

          {_, :nonsplit} ->
            {count - (endx - startx + 1), curr_right}
        end
      end)

    count
  end

  def scan_all(map, yxmap) do
    yxmap
    |> Enum.map(fn {y, xs} ->
      scan_line(map, y, xs)
    end)
    |> Enum.sum()
  end

  def inspect_tile(ty) do
    case ty do
      :ground -> "."
      :v -> "|"
      :h -> "-"
      :tr -> "L"
      :tl -> "J"
      :bl -> "7"
      :br -> "F"
    end
  end
end

map =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Solution.parse_pipe_map()

loop =
  map
  |> Solution.traverse(map.start_point)

loop
|> Enum.count()
|> Solution.must_div_2()
|> (&IO.puts("Part 1: #{&1}")).()

alias Aja.Vector, as: Vec

s = MapSet.new(loop)

scan_res =
  loop
  |> (&Solution.trail_to_yxmap(&1, map)).()
  |> Enum.map(fn {y, xs} ->
    {y, Solution.scan_line(map, y, xs)}
  end)
  |> Enum.into(%{})

for y <- 0..(map.height - 1), x <- 0..(map.width - 1) do
  if MapSet.member?(s, {x, y}) do
    IO.write(Solution.inspect_tile(elem(map[{x, y}], 0)))
  else
    IO.write(".")
  end

  if x == map.width - 1 do
    IO.write(" -> #{scan_res[y]}\n")
  end
end

scan_res
|> Map.values()
|> Enum.sum()
|> (&IO.puts("Part 2: #{&1}")).()
