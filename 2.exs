defmodule Solution do
  @type rgb_dict() :: %{red: integer, green: integer, blue: integer}
  @type game_body() :: [rgb_dict]
  @type game() :: {integer, game_body}

  @spec parse_line(String.t()) :: game
  def parse_line(str) do
    # e.g. "Game 25: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
    # => {25, [{red: 4, green: 0, blue: 3}, {red: 1, green: 2, blue: 6}, {red: 0, green: 2, blue: 0}]}

    re = ~r/^Game (\d+): (.*)$/
    [_, game_n, body] = Regex.run(re, str) || raise "Invalid line"

    {
      String.to_integer(game_n),
      parse_body(body)
    }
  end

  @spec parse_body(String.t()) :: game_body
  def parse_body(str) do
    for parts <- String.split(str, "; ") do
      parse_rgb(parts)
    end
  end

  @spec parse_rgb(String.t()) :: rgb_dict
  def parse_rgb(str) do
    parts = String.split(str, ", ")

    if length(parts) > 3 do
      raise "Too many colors"
    end

    Enum.reduce(parts, %{red: 0, green: 0, blue: 0}, fn part, acc ->
      [count, color] = String.split(part, " ")

      color =
        case color do
          "red" -> :red
          "green" -> :green
          "blue" -> :blue
          _ -> raise "Unknown color"
        end

      if acc[color] != 0 do
        raise "Duplicate color"
      end

      Map.put(acc, color, String.to_integer(count))
    end)
  end

  @spec possible_draw(rgb_dict, rgb_dict) :: boolean
  def possible_draw(rgb_dict, max_rgb_dict) do
    for color <- [:red, :green, :blue] do
      rgb_dict[color] <= max_rgb_dict[color]
    end
    |> Enum.all?()
  end

  @spec possible_game(list(rgb_dict()), rgb_dict()) :: boolean
  def possible_game(game_body, max_rgb) do
    case game_body do
      [] -> true
      [rgb_dict | rest] -> possible_draw(rgb_dict, max_rgb) && possible_game(rest, max_rgb)
    end
  end

  def min_rgb_required(game_body) do
    Enum.reduce(game_body, %{red: 0, green: 0, blue: 0}, fn rgb_dict, acc ->
      [:red, :green, :blue]
      |> Enum.reduce(acc, fn color, acc ->
        Map.put(acc, color, max(rgb_dict[color], acc[color]))
      end)
    end)
  end

  def power_of_rgb(rgb_dict) do
    for color <- [:red, :green, :blue] do
      rgb_dict[color]
    end
    |> Enum.product()
  end
end

parsed_input =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Stream.map(&Solution.parse_line/1)

a_res =
  parsed_input
  |> Stream.filter(fn {_, game_body} ->
    Solution.possible_game(game_body, %{red: 12, green: 13, blue: 14})
  end)
  |> Stream.map(fn {game_n, _} -> game_n end)
  |> Enum.sum()

IO.puts("Part A: #{a_res}")

b_res =
  parsed_input
  |> Stream.map(fn {_, game_body} ->
    Solution.min_rgb_required(game_body)
  end)
  |> Stream.map(&Solution.power_of_rgb/1)
  |> Enum.sum()

IO.puts("Part B: #{b_res}")
