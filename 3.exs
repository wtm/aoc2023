defmodule Solution do
  alias Solution.ScanNumber
  @enforce_keys [:width, :height, :map, :numbers]
  defstruct [:width, :height, :map, :numbers]

  @type t :: %__MODULE__{
          width: pos_integer(),
          height: pos_integer(),
          map: map(),
          numbers: list(Number.t())
        }

  @spec from_lines(list(String.t())) :: t()
  def from_lines(lines) do
    first_line = String.trim_trailing(Enum.at(lines, 0) || raise("No lines provided"), "\n")
    width = String.length(first_line)
    height = Enum.count(lines)

    %{map: map, numbers: numbers} =
      Enum.reduce(Enum.with_index(lines), %{map: %{}, numbers: []}, &do_line/2)

    %__MODULE__{width: width, height: height, map: map, numbers: numbers}
  end

  defmodule Number do
    defstruct [:value, :x, :y, :digit_len]
  end

  @doc """
  Scan a line for consecutive digits and convert them to integers, while
  recording their positions.

  Example:

      iex> scan_line_for_number("..592....#*77.", 3)
      [%Number{value: 592, x: 2, y: 3}, %Number{value: 77, x: 11, y: 3}]

  """
  def scan_line_for_number(line, y) do
    snacc =
      Enum.reduce(line, ScanNumber.init(y), fn char, acc ->
        case char do
          c when c in ?0..?9 ->
            ScanNumber.handle_digit(acc, char)

          _ ->
            ScanNumber.handle_nondigit_char(acc)
        end
      end)
      |> ScanNumber.finalize_number()

    snacc.numbers_rev |> Enum.reverse()
  end

  def do_line({line, y}, %{map: map, numbers: numbers}) do
    line = line |> String.trim_trailing("\n") |> String.to_charlist()
    line_with_index = Enum.with_index(line)

    map =
      Enum.reduce(line_with_index, map, fn {char, x}, map ->
        Map.put(map, {x, y}, char)
      end)

    numbers = numbers ++ scan_line_for_number(line, y)

    %{map: map, numbers: numbers}
  end

  defmodule ScanNumber do
    @type accumulator() :: %{
            numbers_rev: list(),
            curr_number_rev: list(),
            curr_start: non_neg_integer(),
            y: non_neg_integer()
          }

    def init(y) do
      %{numbers_rev: [], curr_number_rev: [], curr_start: 0, y: y}
    end

    @spec finalize_number(accumulator()) :: accumulator()
    def finalize_number(%{
          numbers_rev: numbers_rev,
          curr_number_rev: curr_number_rev,
          curr_start: curr_start,
          y: y
        })
        when curr_number_rev != [] do
      digit_len = Enum.count(curr_number_rev)

      %{
        numbers_rev: [
          %Number{
            value: curr_number_rev |> List.to_string() |> String.reverse() |> String.to_integer(),
            x: curr_start,
            y: y,
            digit_len: digit_len
          }
          | numbers_rev
        ],
        curr_number_rev: [],
        curr_start: curr_start + digit_len,
        y: y
      }
    end

    def finalize_number(acc) when acc.curr_number_rev == [] do
      acc
    end

    @spec handle_digit(accumulator(), char()) :: accumulator()
    def handle_digit(
          acc,
          char
        ) do
      %{
        acc
        | curr_number_rev: [char | acc.curr_number_rev]
      }
    end

    @spec handle_nondigit_char(accumulator()) :: accumulator()
    def handle_nondigit_char(acc) do
      acc =
        case acc.curr_number_rev do
          [] -> acc
          _ -> finalize_number(acc)
        end

      %{
        numbers_rev: acc.numbers_rev,
        curr_number_rev: acc.curr_number_rev,
        curr_start: acc.curr_start + 1,
        y: acc.y
      }
    end
  end

  @spec number_is_part(t(), Number.t()) :: boolean()
  def number_is_part(sch, %{x: x, y: y, digit_len: len}) do
    coords_to_check =
      for i <- -1..len do
        {x + i, y}
      end
      |> Enum.map(fn {x, y} -> [{x, y}, {x, y - 1}, {x, y + 1}] end)
      |> List.flatten()

    coords_to_check
    |> Enum.any?(fn coord -> is_symbol(Map.get(sch.map, coord, ?.)) end)
  end

  def is_symbol(?.), do: false
  def is_symbol(c) when c in ?0..?9, do: false
  def is_symbol(_), do: true

  def find_all_occurrences(sch, char) do
    sch.map
    |> Stream.filter(fn {_, c} -> c == char end)
    |> Enum.map(fn {coord, _} -> coord end)
  end

  def find_adjacent_numbers(sch, {x, y}) do
    sch.numbers
    |> Stream.filter(fn number ->
      xrange = (number.x - 1)..(number.x + number.digit_len)
      yrange = (number.y - 1)..(number.y + 1)
      x in xrange && y in yrange
    end)
  end
end

parsed_schematic =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.filter(&(&1 != ""))
  |> Enum.to_list()
  |> Solution.from_lines()

parsed_schematic.numbers
|> Enum.filter(fn number -> Solution.number_is_part(parsed_schematic, number) end)
|> Enum.map(fn number -> number.value end)
|> Enum.sum()
|> (&IO.puts("Part 1: #{&1}")).()

parsed_schematic
|> Solution.find_all_occurrences(?*)
|> Stream.map(fn coord -> Solution.find_adjacent_numbers(parsed_schematic, coord) end)
|> Stream.filter(fn numbers -> Enum.count(numbers) == 2 end)
|> Enum.map(fn numbers -> Enum.product(Enum.map(numbers, & &1.value)) end)
|> Enum.sum()
|> (&IO.puts("Part 2: #{&1}")).()
