defmodule Progress do
  def print_progress(nb_done, total, curr_val) do
    if nb_done >= total - 1 do
      IO.write("\r\x1b[2K")
    else
      chars = 50
      progress = round(nb_done / total * chars)
      IO.write("\r\x1b[2K#{String.duplicate(">", progress)} #{curr_val}\r")
    end
  end
end

defmodule Grid do
  alias Aja.Vector, as: Vec

  defstruct [:map, :width, :height]

  def parse(lines) do
    width = String.length(Enum.at(lines, 0))
    height = Enum.count(lines)

    map =
      Enum.reduce(Enum.with_index(lines), %{}, fn {line, y}, map ->
        Enum.reduce(Enum.with_index(String.graphemes(line)), map, fn {char, x}, map ->
          entry =
            case char do
              "O" -> :stone
              "." -> :empty
              "#" -> :fixed
            end

          Map.put(map, {x, y}, entry)
        end)
      end)

    %__MODULE__{map: map, width: width, height: height}
  end

  def get_column(grid, x) do
    Enum.map(0..(grid.height - 1), fn y ->
      grid.map[{x, y}]
    end)
  end

  def set_column(grid, x, col) do
    map =
      Enum.reduce(Enum.with_index(col), grid.map, fn {entry, y}, map ->
        Map.put(map, {x, y}, entry)
      end)

    %__MODULE__{grid | map: map}
  end

  def get_row(grid, y) do
    Enum.map(0..(grid.width - 1), fn x ->
      grid.map[{x, y}]
    end)
  end

  def set_row(grid, y, row) do
    map =
      Enum.reduce(Enum.with_index(row), grid.map, fn {entry, x}, map ->
        Map.put(map, {x, y}, entry)
      end)

    %__MODULE__{grid | map: map}
  end
end

defmodule Solution do
  require Logger
  alias Aja.Vector, as: Vec

  def flow_column(col) do
    col = Vec.new(col)

    accumulate =
      Aja.Enum.scan(col, 0, fn entry, acc ->
        case entry do
          :stone -> acc + 1
          _ -> acc
        end
      end)
      |> Enum.with_index()
      |> Enum.map(fn {entry, y} -> {y, entry} end)
      |> Map.new()
      |> Map.put(-1, 0)

    fixed_poses =
      Enum.concat([
        [-1],
        Stream.filter(Aja.Enum.with_index(col), fn {entry, _} -> entry == :fixed end)
        |> Enum.map(fn {_, y} -> y end),
        [Vec.size(col)]
      ])

    rolling_pairs = Enum.zip(fixed_poses, Enum.drop(fixed_poses, 1))

    col =
      Enum.reduce(rolling_pairs, col, fn {start, stop}, col ->
        nb_stones = accumulate[stop - 1] - accumulate[start]

        col =
          Enum.reduce((start + 1)..(start + nb_stones)//1, col, fn y, col ->
            Vec.replace_at(col, y, :stone)
          end)

        Enum.reduce((start + nb_stones + 1)..(stop - 1)//1, col, fn y, col ->
          Vec.replace_at(col, y, :empty)
        end)
      end)

    col
  end

  def flow_column_reverse(col) do
    reversed = Enum.reverse(col)
    flow_column(reversed) |> Enum.reverse()
  end

  def col_load(col) do
    height = Enum.count(col)

    Enum.with_index(col)
    |> Enum.filter(fn {entry, _} -> entry == :stone end)
    |> Enum.map(fn {_, y} -> height - y end)
    |> Enum.sum()
  end

  def solve_part_1(grid) do
    Enum.map(0..(grid.width - 1), fn x ->
      flow_column(Grid.get_column(grid, x)) |> col_load()
    end)
    |> Enum.sum()
  end

  def do_cycle(grid) do
    # North
    grid =
      Enum.reduce(0..(grid.width - 1), grid, fn x, grid ->
        col = Grid.get_column(grid, x)
        col = flow_column(col)
        Grid.set_column(grid, x, col)
      end)

    # West
    grid =
      Enum.reduce(0..(grid.height - 1), grid, fn y, grid ->
        row = Grid.get_row(grid, y)
        row = flow_column(row)
        Grid.set_row(grid, y, row)
      end)

    # South
    grid =
      Enum.reduce(0..(grid.width - 1), grid, fn x, grid ->
        col = Grid.get_column(grid, x)
        col = flow_column_reverse(col)
        Grid.set_column(grid, x, col)
      end)

    # East
    grid =
      Enum.reduce(0..(grid.height - 1), grid, fn y, grid ->
        row = Grid.get_row(grid, y)
        row = flow_column_reverse(row)
        Grid.set_row(grid, y, row)
      end)

    grid
  end

  def solve_part_2(grid) do
    max_cycle = 1_000_000_000

    grid =
      Enum.reduce_while(Stream.cycle([nil]), {grid, %{}, 1}, fn _, {grid, seen_grids, i} ->
        Progress.print_progress(i, 200, "#{i}")
        grid = do_cycle(grid)

        if i == max_cycle do
          {:halt, grid}
        else
          case Map.get(seen_grids, grid.map) do
            nil ->
              {:cont, {grid, Map.put(seen_grids, grid.map, i), i + 1}}

            cycle_start ->
              Progress.print_progress(i, 200, "Cycle detected: #{cycle_start} -> #{i}\n")
              cycle_len = i - cycle_start
              left_to_do = rem(max_cycle - i, cycle_len)
              jmp = max_cycle - left_to_do + 1
              {:cont, {grid, Map.new(), jmp}}
          end
        end
      end)

    Enum.map(0..(grid.width - 1), fn x ->
      Grid.get_column(grid, x) |> col_load()
    end)
    |> Enum.sum()
  end
end

grid =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Enum.filter(&(&1 != ""))
  |> Grid.parse()

IO.puts("Part 1: #{Solution.solve_part_1(grid)}")
IO.puts("Part 2: #{Solution.solve_part_2(grid)}")
