defmodule Progress do
  def print_progress(nb_done, total, curr_val) do
    if nb_done >= total - 1 do
      IO.write("\r\x1b[2K")
    else
      chars = 50
      progress = round(nb_done / total * chars)
      IO.write("\r\x1b[2K#{String.duplicate(">", progress)} #{curr_val}\r")
    end
  end
end

defmodule BeamMap do
  @enforce_keys [:map, :width, :height]
  defstruct [:map, :width, :height]

  def parse_input(lines) do
    lines = Enum.to_list(lines)

    height = Enum.count(lines)
    width = String.length(Enum.at(lines, 0))

    map =
      Enum.reduce(Enum.with_index(lines), %{}, fn {line, y}, map ->
        Enum.reduce(Enum.with_index(String.graphemes(line)), map, fn {char, x}, map ->
          Map.put(map, {x, y}, char)
        end)
      end)

    %__MODULE__{map: map, width: width, height: height}
  end
end

defmodule BeamGraph do
  @moduledoc """
  To solve part 1 in the most efficient manner, we convert the beam map into a
  directed-graph-based representation, where each original tile is represented
  by 4 vertices that may be shared by neighboring tiles, as follows:

   B
  A.D
   C

  In the above example, we also create the following internal edges:

  {A, D}, {B, C}, {D, A}, {C, B}

  This represents that the tile will pass through beams coming from all direction,
  and preserving the original beam direction. Similar edges are created for all
  tile types.
  """

  def from_beam_map(%BeamMap{map: map, width: width, height: height}) do
    graph =
      Enum.reduce(
        0..(height - 1),
        Graph.new(type: :directed, vertex_identifier: fn i -> i end),
        fn y, graph ->
          Progress.print_progress(y, height, "Building beam graph")

          Enum.reduce(0..(width - 1), graph, fn x, graph ->
            tile = map[{x, y}]
            verts = make_vertices(x, y)
            edges = make_int_edges(tile, verts)
            graph |> Graph.add_vertices(verts) |> Graph.add_edges(edges)
          end)
        end
      )

    Enum.reduce(
      0..(height - 1),
      graph,
      fn y, graph ->
        Enum.reduce(0..(width - 1), graph, fn x, graph ->
          graph
          |> Graph.add_edges(make_ext_edges(x, y, width, height))
        end)
      end
    )
  end

  def make_vertices(x, y) do
    [
      # left
      {x, y, :left},
      # top
      {x, y, :top},
      # right
      {x, y, :right},
      # bottom
      {x, y, :bottom}
    ]
  end

  def make_int_edges(tile, [left, top, right, bottom]) do
    case tile do
      "." ->
        [{left, right}, {top, bottom}, {right, left}, {bottom, top}]

      "-" ->
        [{left, right}, {right, left}, {top, left}, {top, right}, {bottom, left}, {bottom, right}]

      "|" ->
        [{top, bottom}, {bottom, top}, {left, top}, {left, bottom}, {right, top}, {right, bottom}]

      "/" ->
        [{left, top}, {top, left}, {right, bottom}, {bottom, right}]

      "\\" ->
        [{left, bottom}, {bottom, left}, {right, top}, {top, right}]
    end
    |> Enum.map(fn {a, b} -> Graph.Edge.new(a, b) end)
  end

  def make_ext_edges(x, y, width, height) do
    [
      {x > 0, {{x - 1, y, :right}, {x, y, :left}}},
      {y > 0, {{x, y - 1, :bottom}, {x, y, :top}}},
      {x < width - 1, {{x, y, :right}, {x + 1, y, :left}}},
      {y < height - 1, {{x, y, :bottom}, {x, y + 1, :top}}}
    ]
    |> Enum.filter(fn {con, _} -> con end)
    |> Enum.flat_map(fn {_, {a, b}} -> [Graph.Edge.new(a, b), Graph.Edge.new(b, a)] end)
  end
end

defmodule Solution do
  def dbg_points(points, width, height) do
    s =
      points
      |> Enum.into(MapSet.new())

    for y <- 0..(height - 1) do
      Enum.reduce(0..(width - 1), "", fn x, acc ->
        if MapSet.member?(s, {x, y}) do
          acc <> "#"
        else
          acc <> "."
        end
      end)
      |> IO.puts()
    end

    points
  end

  def energy_from(graph, vert) do
    Graph.reachable(graph, [vert])
    |> Stream.map(fn {x, y, _t} -> {x, y} end)
    |> Stream.uniq()
    |> Enum.count()
  end

  def solve_part_1(graph) do
    energy_from(graph, {0, 0, :left})
  end

  def solve_part_2(graph, width, height) do
    all_edge_verts =
      Enum.concat([
        Enum.map(0..(height - 1), fn y -> {0, y, :left} end),
        Enum.map(0..(width - 1), fn x -> {x, 0, :top} end),
        Enum.map(0..(height - 1), fn y -> {width - 1, y, :right} end),
        Enum.map(0..(width - 1), fn x -> {x, height - 1, :bottom} end)
      ])

    total = Enum.count(all_edge_verts)

    all_edge_verts
    |> Stream.map(fn v -> {v, energy_from(graph, v)} end)
    |> Stream.with_index()
    |> Stream.map(fn {{v, val}, i} ->
      Progress.print_progress(i, total, "Simulating #{inspect(v)}: #{val}")
      val
    end)
    |> Enum.max()
  end
end

beam_map =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> BeamMap.parse_input()

beam_graph = BeamGraph.from_beam_map(beam_map)

%{width: width, height: height} = beam_map

IO.puts("Part 1: #{Solution.solve_part_1(beam_graph)}")
IO.puts("Part 2: #{Solution.solve_part_2(beam_graph, width, height)}")
