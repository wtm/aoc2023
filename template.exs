defmodule Progress do
  def print_progress(nb_done, total, curr_val) do
    if nb_done >= total - 1 do
      IO.write("\r\x1b[2K")
    else
      chars = 50
      progress = round(nb_done / total * chars)
      IO.write("\r\x1b[2K#{String.duplicate(">", progress)} #{curr_val}\r")
    end
  end
end

defmodule Solution do
  # Stub
end

File.stream!("input", [:utf8, :read_ahead], :line)
|> Stream.map(&String.trim/1)
|> Stream.filter(&(&1 != ""))

# Process input here
