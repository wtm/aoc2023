defmodule IDAssigner do
  defstruct next_id: 1, key_to_ids: %{}

  def new, do: %IDAssigner{}

  def add_key(assigner, key) do
    existing = Map.get(assigner.key_to_ids, key)

    case existing do
      nil ->
        new_id = assigner.next_id

        assigner = %__MODULE__{
          next_id: new_id + 1,
          key_to_ids: Map.put(assigner.key_to_ids, key, new_id)
        }

        {assigner, new_id}

      _ ->
        {assigner, existing}
    end
  end
end

defmodule Grid do
  alias Aja.Vector, as: Vec

  defstruct [:map, :width, :height]

  def from_lines(lines) do
    lines = Enum.map(lines, &String.graphemes/1)
    width = Enum.count(Enum.at(lines, 0))
    height = Enum.count(lines)

    map =
      Enum.reduce(Stream.with_index(lines), %{}, fn {line, y}, map ->
        Enum.reduce(Stream.with_index(line), map, fn {char, x}, map ->
          Map.put(map, {x, y}, char)
        end)
      end)

    %__MODULE__{map: map, width: width, height: height}
  end

  def get_rows_as_ids(grid) do
    Enum.map_reduce(0..(grid.height - 1), IDAssigner.new(), fn y, ida ->
      line =
        Enum.map(0..(grid.width - 1), fn x ->
          Map.get(grid.map, {x, y})
        end)

      {ida, line_id} = IDAssigner.add_key(ida, line)
      {line_id, ida}
    end)
    |> elem(0)
  end

  def get_cols_as_ids(grid) do
    Enum.map_reduce(0..(grid.width - 1), IDAssigner.new(), fn x, ida ->
      line =
        Enum.map(0..(grid.height - 1), fn y ->
          Map.get(grid.map, {x, y})
        end)

      {ida, line_id} = IDAssigner.add_key(ida, line)
      {line_id, ida}
    end)
    |> elem(0)
  end

  def find_reflection_points(list) do
    vec = Vec.new(list)

    Enum.filter(1..(Vec.size(vec) - 1), fn i ->
      wid = min(i, Vec.size(vec) - i)
      Vec.slice(vec, i - wid, wid) == Vec.reverse(Vec.slice(vec, i, wid))
    end)
  end

  def find_reflection_lines(grid) do
    horiz = get_rows_as_ids(grid) |> find_reflection_points()
    vert = get_cols_as_ids(grid) |> find_reflection_points()

    Enum.concat(Enum.map(horiz, &{:h, &1}), Enum.map(vert, &{:v, &1}))
  end

  def flip(grid, {x, y}) do
    map = grid.map |> Map.update({x, y}, nil, &reverse_of/1)
    %__MODULE__{grid | map: map}
  end

  defp reverse_of(char) do
    case char do
      "." -> "#"
      "#" -> "."
    end
  end
end

defmodule Solution do
  alias Aja.Vector, as: Vec

  def split_by_empty_line(lines) do
    Stream.chunk_while(
      lines,
      Vec.new(),
      fn line, acc ->
        case line do
          "" -> {:cont, acc, Vec.new()}
          _ -> {:cont, Vec.append(acc, line)}
        end
      end,
      fn acc -> {:cont, acc, nil} end
    )
    |> Stream.map(fn v -> Aja.Enum.filter(v, fn line -> line != "" end) end)
    |> Stream.filter(fn v -> v != [] end)
  end

  def summarize(rs) do
    case rs do
      [{:h, count}] -> 100 * count
      [{:v, count}] -> count
      _ -> raise "Invalid reflection lines result: #{inspect(rs)}"
    end
  end

  def solve_part_1(grid) do
    rs = Grid.find_reflection_lines(grid)
    summarize(rs)
  end

  def solve_part_2(grid) do
    [orig_ref_line] = Grid.find_reflection_lines(grid)

    new_rs =
      0..(grid.width - 1)
      |> Stream.flat_map(fn x ->
        Stream.map(0..(grid.height - 1), fn y -> {x, y} end)
      end)
      |> Enum.flat_map(fn {x, y} ->
        Grid.flip(grid, {x, y})
        |> Grid.find_reflection_lines()
        |> Enum.filter(&(&1 != orig_ref_line))
      end)
      |> Enum.take(1)

    summarize(new_rs)
  end
end

grids =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Solution.split_by_empty_line()
  |> Enum.map(&Grid.from_lines/1)

grids
|> Enum.map(&Solution.solve_part_1/1)
|> Enum.sum()
|> (&IO.puts("Part 1: #{&1}")).()

grids
|> Enum.map(&Solution.solve_part_2/1)
|> Enum.sum()
|> (&IO.puts("Part 2: #{&1}")).()
