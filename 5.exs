defmodule Solution do
  require Logger
  alias Aja.Vector, as: Vec

  @type map_line() :: {integer(), integer(), integer()}
  @type mapping() :: Vec.t(map_line())

  @spec parse_map(Stream.t(String.t())) :: mapping()
  def parse_map(lines) do
    # We want to sort it in reverse order, so that we can use Enum.find to find
    # the highest mapping that is <= source_idx
    res =
      Stream.map(lines, &parse_line/1)
      |> Vec.new()
      |> Vec.sort(fn {a, _, _}, {b, _, _} -> a >= b end)

    # Quick sanity check for overlapping source indices
    _ =
      Aja.Enum.reverse(res)
      |> Enum.reduce(-1, fn {start, _, len}, curr_max ->
        if start <= curr_max do
          Logger.error("Overlapping source indices found at #{start}")
          Logger.error("Mapping: #{inspect(Aja.Enum.reverse(res))}")
          Logger.error("Current max: #{curr_max}")
          raise "Overlapping source indices found at #{start}"
        else
          start + len - 1
        end
      end)

    res
  end

  def parse_line(line) do
    # line format: destination_start source_start len
    # output: {source_start, destination_start, len}

    [dest_start, source_start, len] =
      String.split(line, " ")
      |> Enum.map(&String.to_integer/1)

    {source_start, dest_start, len}
  end

  def lookup(mapping, source_idx) do
    find_res = Aja.Enum.find(mapping, fn {i, _, _} -> i <= source_idx end)

    case find_res do
      {src_start, dest_start, len} when source_idx < src_start + len ->
        dest_start + (source_idx - src_start)

      _ ->
        source_idx
    end
  end

  def chunk_by_empty_line(lines) do
    Stream.chunk_while(
      lines,
      Vec.new(),
      fn elem, acc ->
        case elem do
          "" -> {:cont, acc, Vec.new()}
          _ -> {:cont, Vec.append(acc, elem)}
        end
      end,
      fn acc -> {:cont, acc, nil} end
    )
  end

  def parse_all_input(lines) do
    [seeds_line | rest] = lines |> Enum.to_list()
    map_chunks = chunk_by_empty_line(rest) |> Enum.filter(&(Vec.size(&1) > 0))
    <<"seeds: ", seeds_line::binary>> = seeds_line
    seeds = seeds_line |> String.split(" ") |> Enum.map(&String.to_integer/1)
    maps = map_chunks |> Enum.map(&Stream.drop(&1, 1)) |> Enum.map(&parse_map/1)
    %{seeds: seeds, maps: maps}
  end

  def forward_propagate(mappings, source_idx) do
    Enum.reduce(mappings, source_idx, &lookup/2)
  end

  def find_min_seeded_loc(seeds, maps) do
    Enum.map(seeds, &forward_propagate(maps, &1))
    |> Enum.min()
  end

  @doc """
  To handle the part 2 of the problem, looking up individual indices is too
  slow. Instead, we can look up ranges of indices at one, which lets us
  preserve continuous ranges keep our complexity at O(length of mapping) per range.
  We return a list of ranges, splitting the range whenever we hit a discontinuity
  in the mapping.
  """
  @spec lookup_range(mapping(), Range.t()) :: list(Range.t())
  def lookup_range(mappings, source_start..source_end) do
    last_idx = Aja.Enum.find_index(mappings, fn {i, _, _} -> i <= source_end end)

    case last_idx do
      nil ->
        # Every mapping is to the right of the source range
        [source_start..source_end]

      last_idx ->
        # Prepare to call _transform_range by reversing the mappings (making
        # them left-to-right) and only taking ones that can possibly overlap,
        # given we've already computed last_idx
        mappings = Vec.slice(mappings, 0, last_idx + 1) |> Aja.Enum.reverse()

        _transform_range(mappings, source_start..source_end)
    end
  end

  defp _transform_one({source_start, dest_start, len}, source_idx) do
    if source_idx < source_start or source_idx >= source_start + len do
      source_idx
    else
      dest_start + (source_idx - source_start)
    end
  end

  @spec _transform_range(list(map_line()), Range.t()) :: list(Range.t())
  defp _transform_range(mappings, source_start..source_end) do
    if source_end < source_start do
      []
    else
      case mappings do
        [] ->
          [source_start..source_end]

        [mapping | rest_mappings] ->
          {map_src_start, _, map_len} = mapping

          cond do
            map_src_start > source_start ->
              split = min(map_src_start - 1, source_end)
              rem_input = (split + 1)..source_end
              [source_start..split | _transform_range(mappings, rem_input)]

            map_src_start + map_len - 1 < source_start ->
              _transform_range(rest_mappings, source_start..source_end)

            true ->
              split = min(map_src_start + map_len - 1, source_end)
              transformed = _transform_one(mapping, source_start).._transform_one(mapping, split)
              rem_input = (split + 1)..source_end
              [transformed | _transform_range(rest_mappings, rem_input)]
          end
      end
    end
  end

  @spec lookup_ranges(mapping(), list(Range.t())) :: list(Range.t())
  def lookup_ranges(mapping, ranges) do
    Enum.reduce(ranges, Vec.new(), fn range, res_ranges ->
      Vec.concat(res_ranges, lookup_range(mapping, range))
    end)
    |> Vec.sort(fn a.._, b.._ -> a <= b end)
    |> Vec.to_list()
  end

  def seeds_to_ranges(seeds_list) do
    case seeds_list do
      [] ->
        []

      [seed_start, len | rem] ->
        [seed_start..(seed_start + len - 1) | seeds_to_ranges(rem)]
    end
  end

  def forward_propagate_range(mappings, source_range) do
    Enum.reduce(mappings, [source_range], &lookup_ranges/2)
  end

  def lowest_location_in_ranges(ranges) do
    ranges |> Enum.map(fn a.._ -> a end) |> Enum.min()
  end
end

%{seeds: seeds, maps: maps} =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Solution.parse_all_input()

IO.puts("Part 1: #{Solution.find_min_seeded_loc(seeds, maps)}")

part_2 =
  Solution.seeds_to_ranges(seeds)
  |> Enum.flat_map(&Solution.forward_propagate_range(maps, &1))
  |> Solution.lowest_location_in_ranges()

IO.puts("Part 2: #{part_2}")
