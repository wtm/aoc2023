defmodule Solution do
  def get_digit(char) when char in ?0..?9, do: char - ?0
  def get_digit(_char), do: nil

  def do_line(line) do
    digits = String.to_charlist(line) |> Enum.map(&get_digit/1) |> Enum.filter(&(&1 != nil))

    if Enum.empty?(digits) do
      0
    else
      first = List.first(digits)
      last = List.last(digits)
      String.to_integer("#{first}#{last}")
    end
  end
end

File.stream!("input", [:utf8, :read_ahead], :line)
|> Enum.map(&Solution.do_line/1)
|> Enum.sum()
|> IO.puts()
