defmodule Solution do
  def parse_input(lines) do
    [times, distances] =
      lines
      |> Enum.map(fn line ->
        [_, nbs] = String.split(line, ":")

        nbs
        |> String.trim()
        |> (&Regex.split(~r/\s+/, &1)).()
        |> Enum.map(&String.to_integer/1)
      end)

    %{times: times, distances: distances}
  end

  def simulate(time_limit, press_time) do
    if time_limit == 0 or press_time >= time_limit do
      0
    else
      go_time = time_limit - press_time
      go_time * press_time
    end
  end

  @doc """
  Retruns %{press_time => distance}
  """
  @spec all_ways(integer()) :: map()
  def all_ways(time_limit) do
    Stream.map(0..time_limit, fn press_time ->
      {press_time, simulate(time_limit, press_time)}
    end)
  end

  def ways_that_work(time_limit, min_distance) do
    all_ways(time_limit)
    |> Stream.filter(fn {_, distance} -> distance >= min_distance end)
  end

  def concat_numbers(numbers) do
    numbers |> Enum.map_join("", &Integer.to_string/1) |> String.to_integer()
  end
end

%{times: times, distances: distances} =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Solution.parse_input()

Enum.zip(times, distances)
|> Enum.map(fn {t, d} -> Solution.ways_that_work(t, d + 1) end)
|> Enum.map(&Enum.count/1)
|> Enum.product()
|> (&IO.puts("Part 1: #{&1}")).()

# Part 2
time = Solution.concat_numbers(times)
distance = Solution.concat_numbers(distances)
IO.puts("Part 2: #{Solution.ways_that_work(time, distance + 1) |> Enum.count()}")
