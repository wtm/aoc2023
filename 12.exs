defmodule Progress do
  def print_progress(nb_done, total, curr_val) do
    if nb_done >= total - 1 do
      IO.write("\r\x1b[2K")
    else
      chars = 50
      progress = round(nb_done / total * chars)
      IO.write("\r\x1b[2K#{String.duplicate(">", progress)} #{curr_val}\r")
    end
  end
end

defmodule Solution do
  defmodule Record do
    @enforce_keys [:conditions, :groups]
    defstruct [:conditions, :groups]
  end

  @spec parse_line(String.t()) :: Record.t()
  def parse_line(line) do
    [conditions, groups] = String.split(line, " ")
    conditions = String.graphemes(conditions) |> Enum.map(&to_symbol/1)
    groups = String.split(groups, ",") |> Enum.map(&String.to_integer/1)

    %Record{
      conditions: conditions,
      groups: groups
    }
  end

  def to_symbol("."), do: :good
  def to_symbol("#"), do: :bad
  def to_symbol("?"), do: :unknown

  def nb_possibilities(%Record{conditions: conditions, groups: groups}) do
    nb_possibilities(conditions, groups, :any, nil)
  end

  defp print_gs(gs) do
    Aja.Enum.map(gs, fn g ->
      case g do
        :good -> "."
        :bad -> "#"
      end
    end)
    |> Enum.join("")
  end

  defp _nb_possibilities_raw(conditions, groups, expect_next, cache) do
    case {conditions, groups, expect_next} do
      {[], [], _} ->
        1

      {[], _, _} ->
        0

      {[:bad | _], [], _} ->
        0

      {cs, [], en} when en in [:good, :any] ->
        if Enum.all?(cs, fn c -> c in [:good, :unknown] end) do
          1
        else
          0
        end

      {[:bad | crest], [g | grest], en} when g > 1 and en in [:bad, :any] ->
        nb_possibilities(crest, [g - 1 | grest], :bad, cache)

      {[:bad | crest], [1 | grest], en} when en in [:bad, :any] ->
        nb_possibilities(crest, grest, :good, cache)

      {[:bad | _], _, :good} ->
        0

      {[:good | crest], [], en} when en in [:good, :any] ->
        nb_possibilities(crest, [], :any, cache)

      {[:good | crest], gs, en} when en in [:good, :any] ->
        nb_possibilities(crest, gs, :any, cache)

      {[:good | _], _, :bad} ->
        0

      {[:unknown | crest], gs, en} ->
        nb_possibilities([:good | crest], gs, en, cache) +
          nb_possibilities([:bad | crest], gs, en, cache)
    end
  end

  def nb_possibilities(conditions, groups, expect_next, cache \\ nil) do
    if cache == nil do
      _nb_possibilities_raw(conditions, groups, expect_next, nil)
    else
      case :ets.lookup(cache, {conditions, groups, expect_next}) do
        [{_, nb}] ->
          nb

        [] ->
          computed = _nb_possibilities_raw(conditions, groups, expect_next, cache)
          :ets.insert(cache, {{conditions, groups, expect_next}, computed})

          computed
      end
    end
  end

  def unfold(%Record{conditions: conditions, groups: groups}) do
    conditions =
      Enum.concat(
        [conditions, conditions, conditions, conditions, conditions]
        |> Enum.intersperse([:unknown])
      )

    groups = Enum.concat([groups, groups, groups, groups, groups])

    %Record{
      conditions: conditions,
      groups: groups
    }
  end
end

records =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Enum.map(&Solution.parse_line/1)

records
|> Stream.map(&Solution.nb_possibilities/1)
|> Enum.sum()
|> (&IO.puts("Part 1: #{&1}")).()

total = Enum.count(records)

records
|> Stream.map(&Solution.unfold/1)
|> Stream.with_index()
|> Stream.map(fn {r, i} ->
  store = :ets.new(nil, [:set, :private])
  res = Solution.nb_possibilities(r.conditions, r.groups, :any, store)
  Progress.print_progress(i, total, res)
  res
end)
|> Enum.sum()
|> (&IO.puts("Part 2: #{&1}")).()
