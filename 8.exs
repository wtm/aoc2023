defmodule Solution do
  require Logger

  require Aja
  alias Aja.Vector, as: Vec

  def parse_input(lines) do
    [instructions | places] = Enum.to_list(lines)
    instructions = String.graphemes(instructions)

    places =
      Enum.map(places, fn line ->
        re = ~r/^(\w+) = \((\w+), (\w+)\)$/
        [_, name, left, right] = Regex.run(re, line)
        {name, {left, right}}
      end)
      |> Enum.into(%{})

    {instructions, places}
  end

  def simulate(instructions, places) do
    Stream.cycle(instructions)
    |> Enum.reduce_while({0, "AAA"}, fn curr_inst, {curr_steps, curr_place} ->
      {next_l, next_r} = places[curr_place] || raise("Unknown place #{curr_place}")

      if curr_place == "ZZZ" do
        {:halt, curr_steps}
      else
        case curr_inst do
          "L" -> {:cont, {curr_steps + 1, next_l}}
          "R" -> {:cont, {curr_steps + 1, next_r}}
        end
      end
    end)
  end

  def simulate_infinite(instructions, places, start) do
    nb_insts = Enum.count(instructions)

    instructions
    |> Stream.with_index()
    |> Stream.cycle()
    |> Stream.scan({start, 0}, fn {curr_inst, curr_inst_id}, {curr_place, _} ->
      {next_l, next_r} = places[curr_place]

      next_place =
        case curr_inst do
          "L" -> next_l
          "R" -> next_r
        end

      next_inst_id = rem(curr_inst_id + 1, nb_insts)

      {next_place, next_inst_id}
    end)
    |> (&Stream.concat([{start, 0}], &1)).()
  end

  def simulate_all(instructions, places_map, starting_places) do
    starting_places
    |> Enum.map(fn place ->
      simulate_infinite(instructions, places_map, place) |> Stream.map(fn {p, _} -> p end)
    end)
  end

  def all_ends_in_z?(names) do
    Enum.all?(names, &String.ends_with?(&1, "Z"))
  end

  def get_starting_places(places_map) do
    places_map |> Map.keys() |> Enum.filter(&String.ends_with?(&1, "A"))
  end

  def solve_part2(instructions, places_map) do
    starting_places = get_starting_places(places_map)
    all_streams = simulate_all(instructions, places_map, starting_places)

    stats_store = :ets.new(nil, [:set, :private])
    :ets.insert(stats_store, {:count, 0})

    Stream.zip(all_streams)
    |> Stream.take_while(fn names ->
      nb_iters = :ets.update_counter(stats_store, :count, 1)

      if rem(nb_iters, 1_000_000) == 0 do
        Logger.debug("Iteration #{nb_iters}: #{inspect(names)}")
      end

      not all_ends_in_z?(Tuple.to_list(names))
    end)
    |> Enum.count()
  end

  def build_zreach_table(instructions, places, starting_place) do
    {z_reaches, cycle_start, cycle_length} =
      simulate_infinite(instructions, places, starting_place)
      |> Enum.reduce_while(
        # Accumulator: {seen, steps, z_reaches}
        #   seen: {place_name, next_inst_id} => step_index
        #   steps: integer
        #   z_reaches: vec([{place_name, step_index}])
        {%{}, 0, Vec.new()},
        fn {curr_place, next_inst_id}, {seen, steps, z_reaches} ->
          cycle_start = Map.get(seen, {curr_place, next_inst_id}, nil)

          if cycle_start != nil do
            Logger.debug("Cycle hit at #{curr_place} (ni = #{next_inst_id}) after #{steps} steps")
            {:halt, {z_reaches, cycle_start, steps - cycle_start}}
          else
            z_reaches =
              if String.ends_with?(curr_place, "Z") do
                Logger.debug(
                  "Found Z at #{curr_place} (ni = #{next_inst_id}) after #{steps} steps"
                )

                Vec.append(z_reaches, {curr_place, steps})
              else
                z_reaches
              end

            {:cont, {Map.put(seen, {curr_place, next_inst_id}, steps), steps + 1, z_reaches}}
          end
        end
      )
  end

  def lcm(a, b) do
    (a * b) |> div(Integer.gcd(a, b))
  end

  def solve_part2_fast(instructions, places_map) do
    starting_places = get_starting_places(places_map)

    branches =
      for p <- starting_places do
        Solution.build_zreach_table(instructions, places_map, p)
      end

    # It appears that we only ever get inputs that happens to specify the
    # special case where the step number at which we reach Z happens to coincide
    # with the cycle length, and we only get this particular Z for each branch.
    # This massively simplifies the calculation because it means that the Z
    # reach will occur at step numbers which are multiples of the cycle length,
    # and so we just need to compute the LCM of all the cycle lengths.

    if not Enum.all?(branches, fn b ->
         match?({Aja.vec([{_, cycle_len}]), _, cycle_len}, b)
       end) do
      raise "This program only works with the specific instance of the problem where the " <>
              "Z reach step number happens to coincide with the cycle length."
    end

    branches
    |> Enum.map(fn {_, _, cycle_len} -> cycle_len end)
    |> Enum.reduce(&Solution.lcm/2)
  end
end

{instructions, places} =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Solution.parse_input()

IO.puts("Part 1: #{Solution.simulate(instructions, places)}")

# IO.puts("Part 2: #{Solution.solve_part2(instructions, places)}")
# Unfortunately this is too slow
IO.puts("Part 2: #{Solution.solve_part2_fast(instructions, places)}")
