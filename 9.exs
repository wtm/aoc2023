defmodule Solution do
  alias Aja.Vector, as: Vec

  def parse_line(line) do
    line
    |> String.split(" ")
    |> Stream.map(&String.to_integer/1)
    |> Vec.new()
  end

  def differentiate(list) do
    Stream.zip(list, Stream.drop(list, 1))
    |> Stream.map(fn {a, b} -> b - a end)
    |> Vec.new()
  end

  def predict(list) do
    diff = differentiate(list)

    if Aja.Enum.all?(diff, &(&1 == 0)) do
      Aja.Enum.at(list, 0)
    else
      predicted_diff = predict(diff)
      Aja.Enum.at(list, -1) + predicted_diff
    end
  end
end

parsed_lines =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Stream.map(&Solution.parse_line/1)

parsed_lines
|> Enum.map(&Solution.predict/1)
|> Enum.sum()
|> (&IO.puts("Part 1: #{&1}")).()

parsed_lines
|> Enum.map(&Aja.Vector.reverse/1)
|> Enum.map(&Solution.predict/1)
|> Enum.sum()
|> (&IO.puts("Part 2: #{&1}")).()
