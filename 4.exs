defmodule Solution do
  require Logger
  alias Aja.Vector, as: Vec

  @spec parse_numbers(String.t()) :: Vec.t(integer())
  def parse_numbers(str) do
    Regex.split(~r/\s+/, str)
    |> Enum.map(&String.trim/1)
    |> Enum.map(&String.to_integer/1)
    |> Vec.new()
  end

  @doc """
  Example:
  "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
  -> %{card_id: 1, winning: vec([41, 48, 83, 86, 17]), have: vec([83, 86, 6, 31, 17, 9, 48, 53])}
  """
  @spec parse_line(String.t()) :: %{
          card_id: integer(),
          winning: Vec.t(integer()),
          have: Vec.t(integer())
        }
  def parse_line(line) do
    try do
      # Regex deliberately written this way to avoid ambiguity (and therefore avoid backtracking)
      re = ~r/^Card +(\d+): +([\d ]*\d) \| ([\d ]*\d)$/
      match = Regex.run(re, line)
      [_, card_id_str, winning_str, have_str] = match

      card_id = String.to_integer(card_id_str)
      winning = winning_str |> String.trim() |> parse_numbers()
      have = have_str |> String.trim() |> parse_numbers()

      %{card_id: card_id, winning: winning, have: have}
    rescue
      e ->
        Logger.critical("Failed to parse line: #{inspect(line)}")
        reraise(e, __STACKTRACE__)
    end
  end

  def nb_winning_numbers(card) do
    Enum.count(card.have, &Enum.member?(card.winning, &1))
  end

  def card_power(card) do
    winning_nbs = nb_winning_numbers(card)

    if winning_nbs >= 1 do
      2 ** (winning_nbs - 1)
    else
      0
    end
  end

  def get_copies_nb(cards) do
    Enum.reduce(cards, %{}, fn card, copies ->
      curr_copies = Map.get(copies, card.card_id, 0) + 1
      copies = Map.put(copies, card.card_id, curr_copies)
      current_win_nb = nb_winning_numbers(card)

      Enum.reduce(1..current_win_nb//1, copies, fn i, copies ->
        Map.update(copies, card.card_id + i, curr_copies, &(&1 + curr_copies))
      end)
    end)
  end
end

cards =
  File.stream!("input", [:utf8, :read_ahead], :line)
  |> Stream.map(&String.trim/1)
  |> Stream.filter(&(&1 != ""))
  |> Enum.map(&Solution.parse_line/1)

cards
|> Enum.map(&Solution.card_power/1)
|> Enum.sum()
|> (fn x -> IO.puts("Part 1: #{x}") end).()

cards
|> Solution.get_copies_nb()
|> Map.values()
|> Enum.sum()
|> (fn x -> IO.puts("Part 2: #{x}") end).()
